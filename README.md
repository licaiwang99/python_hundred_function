# Python 百式觀音

![pic](https://i.imgur.com/MTz6iYs.png)

*每天寫一個滿懷感激的 Function*

## 預計目標

第一階段：
1. 100 種我們常用的 python 寫的功能，節省寶貴回憶與 coding 時間
2. 大致上分成 5 類，內容包含資料讀寫，文字處理，日期處理，快速繪圖等等...

## 更新日誌
    新增 openDataCsv     可快速的根據需求讀取檔案                                                   109/09/02
    新增 PILloadImages 快速讀取資料夾裡的所有圖片                                                   109/09/03
    新增 PILsaveImages 快速轉檔資料夾裡的所有圖片                                                   109/09/03
    新增 getFolder         快速找尋所有資料夾位址                                                   109/09/11
    新增 getFile           快速找尋所有資料夾位址                                                   109/09/11
    新增 openDataXml      讀取指定 tag 的 xml 檔                                                   110/03/20
    新增 tfDatasetGenerator      生成 tfDataset                                                   110/03/23
    新增 tfGpuConfig           檢查與配置顯卡加速                                                  110/03/23
    新增 confusionMatrix            生成混淆矩陣                                                   110/03/23
   


