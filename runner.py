import unittest
import warnings

from dataHandler import (
    PILloadImage,
    PILloadImages,
    PILsaveImages,
    getDataFile,
    openDataCsv,
    openDataXml,
    generateFromDataFrame,
)


from tfHelper import confusionMatrix


class TestDataHandler(unittest.TestCase):
    def test_getDataFile(self):
        result = getDataFile("test.csv", "", "")
        self.assertTrue(result)

    def test_openDataCsv(self):
        result_list = float(openDataCsv("test.csv", returnType="list")[1][1])
        result_frame = openDataCsv("test.csv")["b"][0]
        self.assertEqual(result_list, result_frame)

    def test_PILloadImage(self):
        pic = PILloadImage("test.png")
        warnings.simplefilter("ignore", ResourceWarning)
        self.assertIsNotNone(pic)

    def test_PILloadImages(self):
        pic = PILloadImages("testData//img//icon_png")
        warnings.simplefilter("ignore", ResourceWarning)
        self.assertIsNotNone(pic[0])

    def test_PILsaveImages(self):
        isSucced = PILsaveImages(
            "testData\\img", PILloadImages("testData\\img\\icon_png"), "jpg"
        )
        warnings.simplefilter("ignore", ResourceWarning)
        self.assertTrue(isSucced)

    def test_openDataXml(self):
        result_list = openDataXml(
            "test.xml", tags=["Name", "City", "Zip"], returnType="list"
        )[0][0]
        result_frame = openDataXml("test.xml", tags=["Name", "City", "Zip"])["Name"][0]
        self.assertEqual(result_list, result_frame)

    def test_generateFromDataFrame(self):
        result_frame = openDataCsv("test.csv")
        features, label = (
            [result_frame["a"][0], result_frame["b"][0]],
            result_frame["target"][0],
        )
        dataset = generateFromDataFrame(result_frame, "target")
        datas = [data for data in dataset.take(1).as_numpy_iterator()]
        self.assertEqual(list(map(float, datas[0][0])), features)
        self.assertEqual(int(datas[0][1]), label)


class TesttfHelper(unittest.TestCase):
    def test_confusionMatrix(self):
        y_t = [1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0]
        y_p = [1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0]
        acc, _, _, _ = confusionMatrix(y_t, y_p)
        self.assertEqual(0.75, acc)


if __name__ == "__main__":
    unittest.main(verbosity=2)
