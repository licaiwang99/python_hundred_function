import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt


def confusionMatrix(y_t: list, y_p: list, draw=False):
    """
        Args:

            y_t        (list): 真實的 label
            y_p        (list): 預測的 label

        Returns:
            c_m   (DataFrame):      混淆矩陣

                [ TP ][ FP ]
                [ FN ][ TN ]

            TP: 預測 Yes 實際 YES
            FP: 預測 Yes 實際  NO
            FN: 預測  NO 實際 YES
            TN: 預測  NO 實際  NO

    """
    data = {"y_t": y_t, "y_p": y_p}
    df = pd.DataFrame(data, columns=["y_t", "y_p"])
    c_m = pd.crosstab(df["y_t"], df["y_p"], rownames=["Actual"], colnames=["Predicted"])
    if draw:
        sn.heatmap(c_m, annot=True)
        plt.show()
    acc = (c_m[0][0] + c_m[1][1]) / len(y_t)
    precision = c_m[0][0] / (c_m[0][0] + c_m[0][1])
    recall = c_m[0][0] / (c_m[0][0] + c_m[1][0])
    f1_score = 2 / (1 / precision + 1 / recall)
    return acc, precision, recall, f1_score
