from tensorflow.config.experimental import (
    list_physical_devices,
    set_memory_growth,
    set_virtual_device_configuration,
    VirtualDeviceConfiguration,
)

### 沒有回傳的 function 不會加進 runner.py 裡，僅供參考使用


def gpuSetting(set_memory_growth=True, memory_limit=2048):
    """
    Args:
        set_memory_growth  (Boolean): 是否隨著使用自動增加記憶體
        memory_limit           (Int): 限制記憶體使用總量大小
    """
    gpus = list_physical_devices("GPU")
    if gpus:
        try:
            for gpu in gpus:
                if set_memory_growth:
                    set_memory_growth(gpu, True)
                else:
                    set_virtual_device_configuration(
                        gpus[0],
                        [VirtualDeviceConfiguration(memory_limit=memory_limit)],
                    )
        except RuntimeError as e:
            print(e)
    else:
        print("You don't have GPU !")
