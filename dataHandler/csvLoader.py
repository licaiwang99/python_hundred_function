import csv
import os

import pandas as pd

from .baseLoader import getDataFile


def openDataCsv(
    filename: str,
    foldername="testData//csv",
    roteType="相對",
    encode="utf8",
    returnType="dataFrame",
):
    """
    Args:
        filename    (str): 檔案名稱.
        foldername  (str): 資料夾名稱
        encode      (str): 編碼方式 
        roteType    (str): 路徑方式
        encode      (str): 編碼方式
        returnType  (str): 回傳格式 (list, dataFrame ...)
    Returns:
        讀取的 CSV 檔
    """
    loc,datas = getDataFile(filename, foldername, roteType),[]
    if os.path.isfile(loc):
        if returnType == "dataFrame":
            return pd.read_csv(loc)
        if returnType == "list":
            with open(loc, "r", newline="", encoding=encode) as source:
                for data in csv.reader(source):
                    datas.append(data)
            return datas
    else:
        raise FileNotFoundError
