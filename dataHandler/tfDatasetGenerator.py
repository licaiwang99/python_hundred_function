from tensorflow.data import Dataset


def generateFromDataFrame(df, target: str):
    """
        Args:
            df  (DataFrame): 資料
            traget    (str): label 的名稱
        Returns:
            tensorflow.Dataset
    """
    label = df.pop(target)
    return Dataset.from_tensor_slices((df.values, label.values))
