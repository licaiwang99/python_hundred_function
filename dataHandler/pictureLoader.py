import glob

from PIL import Image
from os.path import splitext
from .baseLoader import getDataFile, getDataFolder


def PILloadImage(name: str) -> Image:
    """
    Args:
        name    (str): 圖片名稱
        
    Returns:
        一張圖片 (PIL)
    """
    try:
        img = Image.open(getDataFile(name, "testData//img", ""))
    except:
        raise FileNotFoundError
    return img


def PILloadImages(foldername: str) -> list:
    """
    Args:
        foldername  (str): 資料夾名稱
        
    Returns:
        圖片陣列    (List[PIL])
    """
    path = getDataFolder(foldername)
    imgs = glob.glob(path + "/*png")
    images = []
    for image in imgs:
        img = Image.open(image)
        images.append(img)
    return images


def PILsaveImages(foldername: str, imgs: Image, img_format: str):
    """
    Args:
        foldername  (str):  資料夾名稱
        imgs        (List): 圖片陣列
        img_format  (str):  儲存格式
    """
    path = getDataFolder(foldername)
    try:
        for i, im in enumerate(imgs):
            if im.mode in ("RGBA", "P"):
                im = im.convert("RGB")
                im.save(f"{path}\\test_{i}.{img_format}")
    except:
        raise TypeError
    return True


