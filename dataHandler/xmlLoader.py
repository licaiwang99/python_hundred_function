import xml.etree.ElementTree as ET
import os

import pandas as pd
from .baseLoader import getDataFile

def openDataXml(
    filename: str,
    tags:list,
    foldername="testData//xml",
    roteType="相對",
    returnType="dataFrame",
):
    """
    Args:
        filename    (str): 檔案名稱.
        tags       (list): 欲擷取的 xml 檔案裡的 tag 
        foldername  (str): 資料夾名稱
        roteType    (str): 路徑方式
        returnType  (str): 回傳格式 dataFrame 或 list
    Returns:
        整理好的 DataFrame 或 list
    """
    loc,datas,col = getDataFile(filename, foldername, roteType),[],[]
    if os.path.isfile(loc):
        roots = ET.parse(loc).getroot()
        for tag in tags:
            elements = [ele.text for ele in roots.findall(f'*/{tag}')]
            datas.append(elements)
            col.append(tag)
        if returnType == "dataFrame":
            # Transpose list, EX: 2X3 -> 3X2
            return pd.DataFrame(map(list, zip(*datas)),columns=col)
    else:
        raise FileNotFoundError
    return datas

    
     
    
