import os


def getDataFile(filename: str, foldername="testData", roteType="") -> str:
    """
    Args:
        filename      (str): 檔案名稱.
        foldername    (str): 資料夾名稱
        roteType      (str): 路徑方式

    Returns:
        路徑 + 檔案名稱.
    """
    if roteType == "絕對":
        dic = os.path.join(os.path.dirname(__file__), foldername)
        return f"{dic}//{filename}"
    else:
        return f"{os.path.realpath('dataHandler')}//{foldername}//{filename}"


def getDataFolder(foldername="testData", roteType="") -> str:
    """
    Args:
        foldername    (str): 資料夾名稱
        roteType      (str): 路徑方式

    Returns:
        路徑
    """
    if roteType == "絕對":
        dic = os.path.join(os.path.dirname(__file__), foldername)
        return f"{dic}"
    if foldername:
        return f"{os.path.realpath('dataHandler')}//{foldername}"
    else:
        raise FileNotFoundError


def getFolder(target, path=os.getcwd()) -> list:
    """
        Args:
            target  (str): 目標
            path    (str): 當前資料夾
        Returns:
            找到的路徑   (list)
    """
    folder = []
    all_dir = os.listdir(path)
    # 當前所有檔案
    for dirs in all_dir:
        for sub_dir in os.walk(dirs):
            if os.path.isdir(sub_dir[0]):
                sep_folder = sub_dir[0].split("\\")
                if target in sep_folder:
                    folder.append(sub_dir[0])
                    print(f"Found target in {sub_dir[0]}")
    return folder


def getFile(target, path=os.getcwd()) -> list:
    """
        Args:
            target  (str): 目標
            path    (str): 當前資料夾
        Returns:
            找到的路徑   (list)
    """
    file_ = []
    all_dir = os.listdir(path)
    # 當前所有檔案
    for dirs in all_dir:
        for sub_dirs in os.walk(dirs):
            for sub_dir in list(sub_dirs[1:]):
                for inf in sub_dir:
                    if target in inf:
                        print(f"Found target file in {sub_dirs[0]}")
                        file_.append(sub_dirs[0] + "\\" + target)
    return file_
